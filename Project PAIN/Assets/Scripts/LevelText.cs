using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelText : MonoBehaviour
{
    public Text levelTxt;
    public Image xpBarImage;
    private PlayerStats pStats;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            pStats.AddExperience(5);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            pStats.AddExperience(50);
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            pStats.AddExperience(500);
        }
    }

    private void SetXPBarSize(float xpNormalized)
    {
        xpBarImage.fillAmount = xpNormalized;
    }

    private void SetLvlNum(int lvlNum)
    {
        levelTxt.text = "LEVEL\n" + (lvlNum + 1);
    }

    public void SetLvl(PlayerStats stats)
    {
        this.pStats = stats;

        //Update the starting values
        SetLvlNum(stats.GetLvlNum());
        SetXPBarSize(stats.GetExperienceNormalized());

        //Subscribe to the changed events
        pStats.OnXPChanged += Stats_OnXPChanged;
        pStats.OnLvlChanged += PStats_OnLvlChanged;
    }

    private void PStats_OnLvlChanged(object sender, System.EventArgs e)
    {
        //Level changed, update text
        SetLvlNum(pStats.GetLvlNum());
    }

    private void Stats_OnXPChanged(object sender, System.EventArgs e)
    {
        //XP changed, update bar size
        SetXPBarSize(pStats.GetExperienceNormalized());
    }
}
