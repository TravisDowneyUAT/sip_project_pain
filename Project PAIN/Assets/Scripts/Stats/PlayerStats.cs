using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public event EventHandler OnXPChanged;
    public event EventHandler OnLvlChanged;

    public Stat maxHealth;
    public int curHealth { get; private set; }

    public Stat maxXP;
    public int curXP { get; private set; }

    public Stat damage;
    public Stat armor;

    private int playerLvl;
    private int xpToNxtLvl;

    public HealthBar healthBar;
    public Image xpBar;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            TakeDamage(0, 10);
        }
    }

    private void Awake()
    {
        curHealth = maxHealth.GetValue();
        healthBar.SetMaxHealth(maxHealth.GetValue());

        playerLvl = 0;
        curXP = 0;
    }

    public void AddExperience(int amt)
    {
        curXP += amt;

        if (curXP >= maxXP.GetValue())
        {
            playerLvl++;
            curXP -= maxXP.GetValue();
            if (OnLvlChanged != null)
            {
                OnLvlChanged(this, EventArgs.Empty);
            }
        }
        if (OnXPChanged != null)
        {
            OnXPChanged(this, EventArgs.Empty);
        }
    }

    public int GetLvlNum()
    {
        return playerLvl;
    }

    public float GetExperienceNormalized()
    {
        return (float)curXP / maxXP.GetValue();
    }

    public void TakeDamage(int damageType, int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        curHealth -= damage;
        Debug.Log(transform.name + " takes " + damage + " damage");

        healthBar.SetHealth(curHealth);

        if (curHealth <= 0)
        {
            Die();
        }

        if(damageType == 0)
        {
            //Take physical damage and run the damage to XP conversion FSM
        }
        if (damageType == 1)
        {
            //Take fire elemental damage and run the damage to XP conversion FSM
        }
        if (damageType == 2)
        {
            //Take poison elemental damage and run the damage to XP conversion FSM
        }
        if (damageType == 3)
        {
            //Take explosive damage and run the damage to XP conversion FSM
        }

    }

    public void Die()
    {
        Debug.Log(transform.name + " died");
    }
}
