using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public Stat maxHealth;
    public int curHealth { get; private set; }

    public Stat damage;
    public Stat armor;
   
    public HealthBar healthBar;

    void Awake()
    {
        curHealth = maxHealth.GetValue();
        healthBar.SetMaxHealth(maxHealth.GetValue());
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            TakeDamage(10);
        }
    }

    public void TakeDamage(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        curHealth -= damage;
        Debug.Log(transform.name + " takes " + damage + " damage");

        healthBar.SetHealth(curHealth);

        if (curHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die()
    {
        Debug.Log(transform.name + " died");
    }


}
