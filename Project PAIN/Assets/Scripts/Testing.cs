using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{
    public PlayerStats pStats;

    [SerializeField] private LevelText lvlTxt;
    void Awake()
    {
        lvlTxt.SetLvl(pStats);
    }
}
